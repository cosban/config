package config

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/cosban/assert"
)

func Test_happy_path(t *testing.T) {
	assert := assert.New(t)
	var given GivenInterface
	actual := launch(&given, happyPath)

	assert.Nil(actual)
}

func Test_invalid_interface(t *testing.T) {
	assert := assert.New(t)
	var given GivenBadInterface
	actual := launch(&given, happyPath)
	expected := fmt.Errorf("%s: Error at %s", invalidOptional, "Plugin.Optional")
	assert.Equals(expected, actual)
}

func Test_ensure_respects_required(t *testing.T) {
	assert := assert.New(t)
	var given GivenInterface
	actual := launch(&given, missingField)
	expected := fmt.Errorf("%s: Error at %s", invalidRequired, "Plugin.Required")

	assert.Equals(expected, actual)
}

func Test_ensure_respects_values(t *testing.T) {
	assert := assert.New(t)
	var given GivenInterface
	actual := launch(&given, wrongValue)
	expected := fmt.Errorf("%s: Error at %s", invalidSelection, "Plugin.Module.Enum")

	assert.Equals(expected, actual)
}

func Test_ensure_respects_required_inside_optional_structs(t *testing.T) {
	assert := assert.New(t)
	var given GivenInterface
	actual := launch(&given, optionalsMissing)
	expected := fmt.Errorf("%s: Error at %s", invalidRequired, "Plugin.OptionalStruct.InnerRequired")

	assert.Equals(expected, actual)
}

func launch(i interface{}, s string) error {
	b := []byte(s)
	json.Unmarshal(b, i)
	if err := validateInterface(i, ""); err != nil {
		return err.ToError()
	}
	return nil
}

type GivenInterface struct {
	Plugin struct {
		Required string
		Optional bool `optional:"true"`
		Module   struct {
			String string
			Enum   string `values:"on,off"`
		}
		OptionalStruct struct {
			InnerRequired string
		} `optional:"true"`
	}
}

type GivenBadInterface struct {
	Plugin struct {
		Required string
		Optional bool `optional:"yes"`
		Module   struct {
			String string
			Enum   string `values:"on,off"`
		}
		OptionalStruct struct {
			InnerRequired string
		} `optional:"true"`
	}
}

const happyPath = `{
    "Plugin": {
		"Required": "value",
		"Module": {
			"String": "value",
			"Enum": "on"
		},
		"OptionalStruct": {
			"InnerRequired": "value"
		}
    }
}`

const missingField = `{
    "Plugin": {
		"Module": {
			"String": "value",
			"Enum": "on"
		}
    }
}`

const wrongValue = `{
    "Plugin": {
		"Required": "value",
		"Module": {
			"String": "value",
			"Enum": "yellow"
		}
    }
}`

const optionalsMissing = `{
    "Plugin": {
		"Required": "value",
		"Module": {
			"String": "value",
			"Enum": "on"
		},
		"OptionalStruct": {
		}
    }
}`
