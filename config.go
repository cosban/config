package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
	"strconv"
	"strings"
)

func ReadConfig(i interface{}) error {
	return ReadConfigAt(i, "config.json")
}

func ReadConfigAt(i interface{}, path string) error {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(b, i); err != nil {
		return err
	}
	if err := validateInterface(i, ""); err != nil {
		return err.ToError()
	}
	return nil
}

func validateInterface(i interface{}, path string) *ConfigError {
	t := reflect.TypeOf(i)
	val := reflect.ValueOf(i)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
		val = val.Elem()
	}
	if err := validateFields(t, val, path); err != nil {
		return err
	}
	return nil
}

func validateFields(t reflect.Type, rv reflect.Value, base string) *ConfigError {
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		val := rv.Field(i)

		name := getName(f)
		path := name
		if base != "" {
			path = fmt.Sprintf("%s.%s", base, name)
		}

		required := true
		optional := f.Tag.Get("optional")
		if len(optional) > 0 {
			if value, err := strconv.ParseBool(optional); err != nil {
				return ErrInvalidOptional(path)
			} else {
				required = !value
			}
		}
		if err := validateRequiredField(required, f, val, path); err != nil {
			return err
		}
		if val.Kind() == reflect.Struct && len(val.String()) > 0 {
			if err := validateInterface(val.Interface(), path); err != nil {
				return err
			}
		}
		if err := validateSelectionField(f, val, path); err != nil {
			return err
		}
	}
	return nil
}

func validateRequiredField(required bool, f reflect.StructField, val reflect.Value, path string) *ConfigError {
	if required && len(val.String()) < 1 {
		return ErrInvalidRequired(path)
	}
	return nil
}

func validateSelectionField(f reflect.StructField, rv reflect.Value, path string) *ConfigError {
	selection := f.Tag.Get("values")
	if len(selection) == 0 {
		return nil
	}
	for _, v := range strings.Split(selection, ",") {
		if strings.TrimSpace(v) == rv.String() {
			return nil
		}
	}
	return ErrInvalidSelection(path)
}

func getName(f reflect.StructField) string {
	json := f.Tag.Get("json")
	if json == "" {
		return f.Name
	}
	name := strings.Split(json, ",")[0]
	if name == "" || name == "-" || name == "*" {
		return f.Name
	}
	return name
}
