package config

import (
	"errors"
	"fmt"
)

const (
	invalidOptional  = "Invalid optional tag. This is a problem in the code, not the configuration"
	invalidRequired  = "A required field was left blank"
	invalidSelection = "Invalid parameter used from predefined selection"
)

type ConfigError struct {
	err  string
	path string
}

func ErrInvalidOptional(path string) *ConfigError {
	return &ConfigError{
		err:  invalidOptional,
		path: path,
	}
}

func ErrInvalidRequired(path string) *ConfigError {
	return &ConfigError{
		err:  invalidRequired,
		path: path,
	}
}

func ErrInvalidSelection(path string) *ConfigError {
	return &ConfigError{
		err:  invalidSelection,
		path: path,
	}
}

func (err *ConfigError) ToError() error {
	message := fmt.Sprintf("%s: Error at %s", err.err, err.path)
	return errors.New(message)
}
