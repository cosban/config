# config
> A configuration file to interface utility

## Overview

This package allows you to read and validate json files as a configuraton in order to bring your program up to a working 
state. 

## Installation

`go get gitlab.com/cosban/config`

## Usage

In order to start, you must have a configuration interface available such as the following example:

config.json
```
{
    "host": {
		"port": 80,
		"ip": "127.0.0.1",
    },
    "sol": {
		"mode": "prefer",
		"sql": {
			"host": "localhost",
			"port": 5432,
			"database": "data",
			"username": "name",
			"mode": "disable"
		}
	}
}
```

config.go
```
// define the config struct
type Configuration struct {
	Host struct {
		IP       string `json:"ip"` // standard json tag
		Port     int    `json:"port"`
	} `json:"host"`
	State struct {
		Mode string     `json:"mode" values:"disabled,required,prefer"`  
        // the values tag defines all of the valid values for this field. Any other values will cause a panic.
		Sql      struct {
			Host     string `json:"host"`
			Port     int    `json:"port"`
			Password string `json:"password" optional:"true"`
            // if optional is true (default: false) then the field does not have to be defined.
			Database string `json:"database"`
			Username string `json:"username"`
			Mode     string `json:"sslmode" values:"disable,allow,prefer,require,verify-ca,verify-full"` 
		} `json:"sql"`
	} `json:"state"`
}

// create empty config struct
var config Configuration
```

Once you have your configuration interface defined, you may simply ask this package to read the json file and populate 
the struct with values from the file.

main.go
```
import "gitlab.com/cosban/config"

func main() {
    err := config.ReadConfig(&config, "config.json")
    if err != nil {
        panic(err)
    }
    fmt.Println(config)
}
```

Use this struct as you wish. It'll be filled with the values assigned from your file.